#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(){
	int n1, n2, i,r1,r2;
	FILE *fp = fopen("web-Stanford.txt","r");
	FILE *fp2 = fopen("web-Stanford_node.csv","w");
	char ar[1000];
	int node [282000];

	srand(time(NULL));

//	printf("%d\n",RAND_MAX);

	//memset(node,0,4*282000);
	//memset(node, 0, 282000*sizeof(node[0])); 
	for(i=0;i<282000;i++) node[i] = 0;

	fgets(ar,1000,fp);
	fgets(ar,1000,fp);
	fgets(ar,1000,fp);
	fgets(ar,1000,fp);

	fprintf(fp2,"id,size,friendliness\n");
//12497
	for(i=0;i<2312497;i++) {
		fscanf(fp,"%d %d",&n1, &n2);
		//printf("n1: %d, n2:%d, node[n1]: %d, node[n2]: %d\n",n1,n2,node[n1],node[n2]);
		if (node[n1] == 0){
			node[n1] = 1;
			r1 = rand()%100000;
			r2 = rand()%10;
			fprintf(fp2,"%d,%d,%d\n",n1,r1,r2);
			
		}
		if (node[n2] == 0){
			node[n2] = 1;
			r1 = rand()%100000;
			r2 = rand()%10;
			fprintf(fp2,"%d,%d,%d\n",n2,r1,r2);
			
		}
	}
	fclose(fp);
	fclose(fp2);
	//Creating random input files:
	//For 500 keys
	FILE *f500 = fopen("key500.csv","w");
	fprintf(f500,"search_nodes\n");
	for(i=0;i<500;i++){
		r1 = rand()%281904;
		fprintf(f500,"%d\n",r1);
	}
	fclose(f500);

	//For 1000 keys
	FILE *f1000 = fopen("key1000.csv","w");
	fprintf(f1000,"search_nodes\n");
	for(i=0;i<1000;i++){
		r1 = rand()%281904;
		fprintf(f1000,"%d\n",r1);
	}
	fclose(f1000);

	//For 2000 keys
	FILE *f2000 = fopen("key2000.csv","w");
	fprintf(f2000,"search_nodes\n");
	for(i=0;i<2000;i++){
		r1 = rand()%281904;
		fprintf(f2000,"%d\n",r1);
	}
	fclose(f2000);

	//For 5000 keys
	FILE *f5000 = fopen("key5000.csv","w");
	fprintf(f5000,"search_nodes\n");
	for(i=0;i<5000;i++){
		r1 = rand()%281904;
		fprintf(f5000,"%d\n",r1);
	}
	fclose(f5000);

	//For 10000 keys
	FILE *f10000 = fopen("key10000.csv","w");
	fprintf(f10000,"search_nodes\n");
	for(i=0;i<10000;i++){
		r1 = rand()%281904;
		fprintf(f10000,"%d\n",r1);
	}
	fclose(f10000);

	//For 15000 keys
	FILE *f15000 = fopen("key15000.csv","w");
	fprintf(f15000,"search_nodes\n");
	for(i=0;i<15000;i++){
		r1 = rand()%281904;
		fprintf(f15000,"%d\n",r1);
	}
	fclose(f15000);

	//For 20000 keys
	FILE *f20000 = fopen("key20000.csv","w");
	fprintf(f20000,"search_nodes\n");
	for(i=0;i<20000;i++){
		r1 = rand()%281904;
		fprintf(f20000,"%d\n",r1);
	}
	fclose(f20000);
return 0;
}
