#install rust
curl https://sh.rustup.rs -sSf | sh

#add cargo to the path
export PATH="$HOME/.cargo/bin:$PATH"

#build the complete replication file
cargo build --release --package pro848Timely --bin copy_all2

#build the row partitioned file
cargo build --release --package pro848Timely --bin part_row2

#build the column partitioned file
cargo build --release --package pro848Timely --bin part_col

#run the complete replication file with #hop_count #query #worker thread (rusn in a single process)
./target/release/copy_all2 3 500 -w12

#run the row partitioned file with #hop_count #query #worker thread (rusn in a single process)
./target/release/part_row2 3 500 -w12

#run the column partitioned file with #hop_count #query #worker thread (rusn in a single process)
./target/release/part_col 3 500 -w12

#To run in multiple process, two terminal needs to be opened with each giving the following commands:
#terminal 1 $ ./target/release/copy_all2 3 500 -w6 -n2 -p0
#terminal 2 $ ./target/release/copy_all2 3 500 -w6 -n2 -p1
#here n is the number of proceses and p is the process index
#similarly to run in multiple server host.txt needs to be configured as:
#
# IP1:port1
# IP2:port2
#
#and the command at the terminal of one servers is:
#terminal 1 $ ./target/release/copy_all2 3 500 -w6 -n2 --hostfile host.txt -p0
#and the other server is:
#terminal 2 $ ./target/release/copy_all2 3 500 -w6 -n2 --hostfile host.txt -p1
