extern crate timely;
extern crate csv;
#[macro_use]
extern crate serde_derive;

use std::sync::{RwLock, Arc};
use rand::Rng;
use timely::dataflow::operators::{Filter, Input, Map, Inspect, Probe, Operator, Feedback};
use timely::dataflow::{InputHandle, ProbeHandle};
use std::ops::Deref;
use timely::dataflow::channels::pact::{Exchange, Pipeline};
use std::hash::{Hash, Hasher};
use std::collections::hash_map::DefaultHasher;

#[derive(Debug, Clone, Deserialize)]
struct Record{
    person: String,
    city: String,
    province: String,
    age: usize,
    salary: u64,
}

fn search_graph(graph: Arc<RwLock<Vec<(Record,Record)>>>, key: String, hop: usize) {

    timely::execute(timely::Configuration::Process(3),move |worker|{

        let data= Arc::clone(&graph);
        let ad_list = Arc::clone(&adj);
        let nodes = Arc::clone(&nodes);
        let search_string = key.clone();
        let mut input = InputHandle::new();
        let mut probe = ProbeHandle::new();
        let index = worker.index();
        let peers = worker.peers();

        let (left_index, right_index) = get_worker_indices(data.read().unwrap().len(),index,peers);
        //let h = get_hashcode(key.clone()) % peers;

        worker.dataflow::<usize,_,_>( |scope|{

            let data2= Arc::clone(&graph);
            let data3= Arc::clone(&graph);

            scope.input_from(&mut input)
                .unary_frontier(
                    Exchange::new(move |x| index as u64),
                    "search_query",
                    |cap,info|{
                        move |input,output| {

                            while let Some(val) = input.next() {
                                let (a, b) = val; //(timely::dataflow::operators::CapabilityRef<'_, usize>, timely::communication::message::RefOrMut<'_, std::vec::Vec<_>>)
                                let mut session = output.session(&a);
                                let mut result: Vec<Record> = Vec::new();
                                let mut buffer: Vec<Record> = Vec::new();
                                for ind in b.iter() {
                                    //println!("data is {:?}", ind);
                                    let gr = data.read().unwrap();

                                    //println!("{:?}",gr.get(*ind as usize).unwrap().0);
                                    let index: usize = *ind as usize;

                                    if gr.deref().get(index).unwrap().deref().0.person == search_string {
                                        buffer.push(gr.deref().get(index).unwrap().1.clone());
                                        result.push(gr.deref().get(index).unwrap().1.clone());
                                        let mut temp: Vec<Record> = vec![];
                                        for i in 1..(hop-1) {
                                            while !buffer.is_empty() {
                                                let record = buffer.pop().unwrap();
                                                let mut loc :usize = 0;
                                                for i in node.iter() {
                                                    if(i.person == record.person){
                                                        for j in list[loc].iter() {
                                                            temp.push(node[*j].clone());
                                                        }
                                                    }
                                                    loc+=1;
                                                }
                                            }
                                            buffer.append(&mut temp);
                                            for i in buffer.iter() {
                                                result.push(i.clone());
                                            }
                                        }
                                    }
                                    for i in result.iter(){
                                        if i.city == "Waterloo".to_string() {
                                            println!("Final result: {:?}",i);
                                            session.give(index);
                                        }
                                    }
                                }
                            }
                        }
                    })
                .probe_with(&mut probe);
        });

        for x in left_index..right_index{
            input.send(x);
        }

        input.advance_to(1);
        while probe.less_than(input.time()){
            worker.step();
        }
    }).unwrap();
}


fn main() {
    //command-line arguments. No of threads and search key
    let key: String = std::env::args().nth(1).unwrap().parse().unwrap();
    let hop: usize = std::env::args().nth(2).unwrap().parse().unwrap();

    let mut rdr = csv::Reader::from_path("data.csv").unwrap();
    let mut data=Vec::new();
    for result in rdr.deserialize() {
        let record: Record = result.unwrap();
        data.push(record);
    }
    //Guess the number of edges in the graph
    let edges = ((data.len() as f64)* 5.0 ) as usize;

    //create the graph from the data
    let mut rng = rand::thread_rng();
    let mut list = vec![0 as usize;data.len() as usize];
    //let mut ad_list= vec![[0usize; 1]; data.len()];
    let mut ad_list:Vec<Vec<usize>> = Vec::new();
    ad_list = vec![Vec::new();data.len() as usize];

    let graph:Vec<(Record,Record)> = (0..edges)
        .map(|_| {
            let i = rng.gen_range(0u32, data.len() as u32) as usize;
            let j = rng.gen_range(0u32, data.len() as u32) as usize;
            let x: Record = data.get(i).unwrap().clone();
            let y: Record  = data.get(j).unwrap().clone();
            ad_list[i].push(j);
            ad_list[j].push(i);
            (x,y)
        })
        .collect();


    //let data= Arc::new(Mutex::new(data));
    let gr = Arc::new(RwLock::new(graph));
    let adj = Arc::new(RwLock::new(ad_list));
    let nodes = Arc::new(RwLock::new(data));

    //let gr=Arc::clone(&graph);
    //search_graph(gr,nodes, adj,key,hop);
    search_partitioned(gr,key,hop);
}

pub fn get_worker_indices(
    total_len: usize,
    worker_index: usize,
    worker_count: usize,
) -> (usize, usize) {
    let data_per_worker = total_len / worker_count;
    let left_index = data_per_worker * worker_index;
    let right_index = if (worker_index + 1) == worker_count {
        // Handle extra elements.
        total_len
    } else {
        data_per_worker * (worker_index + 1)
    };
    (left_index, right_index)
}

fn get_hashcode(key: String) -> u64 {
    let mut hasher = DefaultHasher::new();
    key.hash(&mut hasher);
    hasher.finish()
}