extern crate timely;
extern crate csv;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate abomonation_derive;

use std::sync::{RwLock, Arc};
use rand::Rng;
use timely::dataflow::operators::{Input, Inspect, Probe, Operator, ConnectLoop, Concat, Feedback, Filter, Exchange};
use timely::dataflow::{InputHandle, ProbeHandle};
use std::ops::Deref;
use timely::dataflow::channels::pact::Pipeline;
use std::hash::{Hash, Hasher};
use std::collections::hash_map::DefaultHasher;
use std::time::{SystemTime, Instant};
use std::collections::HashMap;
use pro848Timely::{AdjListPtr, Record, Link, AdjList, Vertices, VerticesPtr, get_data, get_keys};

fn search_partitioned(ad_list: AdjListPtr, nodes: VerticesPtr, keys: Vec<usize>, hop: usize) {

    //timely::execute(timely::Configuration::Process(12),move |worker|{
      timely::execute_from_args(std::env::args(), move |worker| {

        let index = worker.index();
        let peers = worker.peers();

        let hop_count= hop.clone();
        //let (left_index, right_index) = get_worker_indices(data.read().unwrap().len(),index,peers);

          //let timer = Instant::now();
          for search_key in &keys {
              let mut input = InputHandle::new();
              worker.dataflow::<usize, _, _>(|scope| {
                  let (handle, cycle) = scope.feedback(1);
                  //counter +=1;
                  scope.input_from(&mut input)
                      //.exchange(|x :&(Record, usize)| index as u64)
                      .inspect(|x: &(usize, usize)| {})
                      .concat(&cycle)
                      .unary_frontier(
                          Pipeline,
                          "partitioned graph",
                          |cap, info|
                              {
                                  move |input, output| {
                                      while let Some((a, b)) = input.next() {
                                          let mut session = output.session(&a);
                                          for str_in in b.iter() {
                                              let mut loc: usize = str_in.0;
//                                for i in node.iter() {
//                                    if i.person == *str_in.deref().0.person {
//                                        break;
//                                    }
//                                    loc += 1;
//                                }
                                              //println!("{:?}",gr.get(*ind as usize).unwrap().0);
                                              for ind in ad_list.list[loc].iter() {
                                                  session.give((*ind, str_in.1 + 1 as usize));
                                              }
                                          }
                                      }
                                  }
                              })
                      .filter(move |x: &(usize, usize)| {
                          //println!("node: {:?}, counter: {}",x.0 ,x.1);
                          let ind = x.0;
                          let rec = nodes.records[ind].clone();
                          if (rec.size > 10000) && (rec.friendliness > 5) {
                              //                  println!("value: {}, {:?}",ind,rec); //results at filter condition
                          }
                          if x.deref().1 < hop_count
                          { true } else { false }
                      })
                      .exchange(move |x: &(usize, usize)| {
                          let step = 281903 / peers;
                          let mut temp = x.0 / step;
                          if temp == peers { temp -= 1; }
                          temp as u64
                      })
                      //.delay(|_,_| 100)
                      .connect_loop(handle);
                  //.probe_with(&mut probe);
              });

              let step = 281903 / peers;

              let mut temp = search_key / step;
              if temp == peers { temp -= 1; }
              if (index == temp) {
                  input.send((*search_key, 0 as usize));
              }
          }
          //if index == 0{
          // }

      }).unwrap();
}


fn main() {
    let (ad_list,data,hop,keys) = get_data();

    let mut rng = rand::thread_rng();

    //let all_keys = (0..keys).map(|key| rng.gen_range(1u32, 281903u32) as usize).collect();
    let all_keys = get_keys(keys);

    let timer = Instant::now();
    search_partitioned(AdjListPtr::new(&ad_list), VerticesPtr::new(&data), all_keys, hop);
    println!("part_row: For searching: {} nodes with {} hop, it took {:?}",keys.len(),hop,timer.elapsed());
}
