extern crate timely;
extern crate csv;

use std::sync::{RwLock, Arc};
use rand::Rng;
use timely::dataflow::operators::{Filter, Input, Map, Inspect, Probe, Operator, Feedback, Exchange, Concat, ConnectLoop};
use timely::dataflow::{InputHandle, ProbeHandle};
use std::ops::Deref;
use timely::dataflow::channels::pact::Pipeline;
use std::hash::{Hash, Hasher};
use std::collections::hash_map::DefaultHasher;
use std::time::{SystemTime, Instant};
use std::collections::HashMap;
use pro848Timely::{AdjListPtr, Record, Link, AdjList, Vertices, VerticesPtr, get_data, get_keys};

fn search_graph(ad_list: AdjListPtr, nodes: VerticesPtr, keys: Vec<usize>, hop: usize) {

    //timely::execute(timely::Configuration::Process(1),move |worker|{
    timely::execute_from_args(std::env::args(), move |worker| {
        // let mut probe = ProbeHandle::new();
        let index = worker.index();
        let peers = worker.peers();

        // let (left_index, right_index) = get_worker_indices_base(index,peers);

      //  let timer = Instant::now();
        for search_key in &keys {
            let mut input = InputHandle::new();
            worker.dataflow::<usize,_,_>( |scope|{

                let (handle, cycle) = scope.feedback(1);

                scope.input_from(&mut input)
                    .inspect(|x: &(usize,usize)| {})
                    .concat(&cycle)
                    .unary_frontier(
                        Pipeline,
                        "search_query",
                        |cap,info|{
                            move |input,output| {

                                while let Some(val) = input.next() {
                                    let (a, b) = val;
                                    let mut session = output.session(&a);
                                    for rec in b.iter() {
                                        let mut loc: usize = rec.0;
                                        //                                    for i in node.iter() {
                                        //                                        if i.person == *rec.deref().0.person {
                                        //                                            break;
                                        //                                        }
                                        //                                        loc+=1;
                                        //                                    }
                                        for i in ad_list.list[loc].iter() {
                                            //println!("given id:{}, connected id: {}",loc, i);
                                            session.give((*i,(rec.1+1) as usize ));
                                        }
                                    }
                                }
                            }
                        })
                    .filter(move|x: &(usize,usize)| {
                        let ind = x.0;
                        let rec = nodes.records[ind].clone();
                        //                     if(rec.size > 10000) && (rec.friendliness > 5) {
                        //                         println!("{:?}",rec); //results at filter condition
                        //                     }
                        if (x.deref().1 < hop)
                        {true}
                        else {false}
                    })
                    /*                .exchange(move |x: &(usize,usize)| {
                                        let step = 281903/peers;
                                        let mut temp = x.0 / step;
                                        if temp == peers { temp -= 1; }
                                        temp as u64
                                    })
                    */
                    .connect_loop(handle);
            });

            let step = 281903/peers;

            let mut temp = search_key / step;
            if temp == peers { temp -= 1; }
            if(index == temp) {
                input.send((*search_key,0 as usize));
            }

        }
        //if index == 0{

       // }

    }).unwrap();
}

fn main() {
    let (ad_list,data,hop,keys) = get_data();

    let mut rng = rand::thread_rng();

    //let all_keys = (0..keys).map(|key| rng.gen_range(1u32, 281903u32) as usize).collect();
    let all_keys = get_keys(keys);

    let timer = Instant::now();
    search_graph(AdjListPtr::new(&ad_list), VerticesPtr::new(&data), all_keys, hop);
    println!("copy_all: For searching: {} nodes with {} hop, it took {:?}",keys.len(),hop,timer.elapsed());
}
