extern crate timely;
extern crate csv;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate abomonation_derive;

use std::sync::{RwLock, Arc};
use rand::Rng;
use timely::dataflow::operators::{Input, Inspect, Probe, Operator, ConnectLoop, Concat, Feedback, Filter, Broadcast, Delay};
use timely::dataflow::{InputHandle, ProbeHandle};
use std::ops::Deref;
use timely::dataflow::channels::pact::Pipeline;
use std::hash::{Hash, Hasher};
use std::collections::hash_map::DefaultHasher;

#[derive(Abomonation, Debug, Clone, Deserialize)]
struct Record{
    person: String,
    city: String,
    province: String,
    age: usize,
    salary: u64,
}

fn search_partitioned(graph: Arc<RwLock<Vec<(Record,Record)>>>, key: Record, hop: usize) {

    timely::execute(timely::Configuration::Process(3),move |worker|{
        let data= Arc::clone(&graph);
        let search_key = key.to_owned();
        let mut input = InputHandle::new();
        let mut probe = ProbeHandle::new();
        let index = worker.index();
        let peers = worker.peers();
        let mut counter: usize = 0;

        let (left_index, right_index) = get_worker_indices(data.read().unwrap().len(),index,peers);

        worker.dataflow::<usize,_,_>( |scope| {

            let (handle, cycle) = scope.feedback(1);
            //counter +=1;
            scope.input_from(&mut input)
                .concat(&cycle)
                .inspect(move|x: &(Record,usize)| println!("input found: {:?} in thread {}",x,index))
                .unary_frontier(Pipeline, "partitioned graph",|cap, info|{
                    move |input, output|{
                        while let Some((a,b)) = input.next() {
                            let mut session = output.session(&a);
                            for str_in in b.iter() {
                                let gr = data.read().unwrap();
                                //println!("{:?}",gr.get(*ind as usize).unwrap().0);
                                for ind in left_index..right_index {
                                    if gr.deref().get(ind).unwrap().deref().0.person == str_in.deref().0.person {
                                        println!("Found edge from source: {:?} at thread {} on round {}",gr.deref().get(ind).unwrap(),index,str_in.deref().1);
                                        session.give((gr.deref().get(ind).unwrap().deref().1.clone(),str_in.deref().1+1 as usize));
                                    }
                                }
                            }
                        }
                    }
                })
                .broadcast()
                .filter(move|x: &(Record,usize)| {
                    println!("counter: {}, hop: {}",x.deref().1, hop);
                    if x.deref().1 < hop {true}
                    else {false}
                })
                //.delay(|_,_| 100)
                .connect_loop(handle);
                //.probe_with(&mut probe);
        });
        //for x in left_index..right_index{
        input.send((search_key,0 as usize));
        //}

        input.advance_to(1);
        while probe.less_than(input.time()){
            worker.step();
        }
    }).unwrap();
}

fn main() {
    //command-line arguments. No of threads and search key
    let key: String = std::env::args().nth(1).unwrap().parse().unwrap();
    let hop: usize = std::env::args().nth(2).unwrap().parse().unwrap();

    let mut rdr = csv::Reader::from_path("data.csv").unwrap();
    let mut data=Vec::new();
    for result in rdr.deserialize() {
        let record: Record = result.unwrap();
        data.push(record);
    }
    //Guess the number of edges in the graph
    let edges = ((data.len() as f64)* 1.5 ) as usize;

    //create the graph from the data
    let mut rng = rand::thread_rng();
    let mut list = vec![0 as usize;data.len() as usize];
    //let mut ad_list= vec![[0usize; 1]; data.len()];
    //let mut ad_list:Vec<Vec<usize>> = Vec::new();
    //ad_list = vec![Vec::new();data.len() as usize];

    let graph:Vec<(Record,Record)> = (0..edges)
        .map(|_| {
            let i = rng.gen_range(0u32, data.len() as u32) as usize;
            let j = rng.gen_range(0u32, data.len() as u32) as usize;
            let x: Record = data.get(i).unwrap().clone();
            let y: Record  = data.get(j).unwrap().clone();
            (x,y)
        })
        .collect();

        let mut result = None;
        for i in data.iter() {
            if i.person == key {
                result = Some(i);
                break;
            }
        }
        let key: Record = result.expect("Key not found").clone();

    //let data= Arc::new(Mutex::new(data));
    let gr = Arc::new(RwLock::new(graph));
    //let adj = Arc::new(RwLock::new(ad_list));
    //let nodes = Arc::new(RwLock::new(data));

    //let gr=Arc::clone(&graph);
    search_partitioned(gr,key,hop);
}

pub fn get_worker_indices(
    total_len: usize,
    worker_index: usize,
    worker_count: usize,
) -> (usize, usize) {
    let data_per_worker = total_len / worker_count;
    let left_index = data_per_worker * worker_index;
    let right_index = if (worker_index + 1) == worker_count {
        // Handle extra elements.
        total_len
    } else {
        data_per_worker * (worker_index + 1)
    };
    (left_index, right_index)
}

fn get_hashcode(key: String) -> u64 {
    let mut hasher = DefaultHasher::new();
    key.hash(&mut hasher);
    hasher.finish()
}