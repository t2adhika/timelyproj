extern crate timely;
extern crate csv;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate abomonation_derive;

use std::sync::{RwLock, Arc};
use rand::Rng;
use timely::dataflow::operators::{Input, Inspect, Probe, Operator, ConnectLoop, Concat, Feedback, Filter, Exchange};
use timely::dataflow::{InputHandle, ProbeHandle};
use std::ops::Deref;
use timely::dataflow::channels::pact::Pipeline;
use std::hash::{Hash, Hasher};
use std::collections::hash_map::DefaultHasher;
use std::collections::HashMap;
use std::time::{SystemTime, Instant};
use pro848Timely::{AdjListPtr, Record, Link, AdjList, Vertices, VerticesPtr, get_data, get_keys};

fn search_partitioned(ad_list: AdjListPtr, nodes: VerticesPtr, keys: Vec<usize>, hop: usize) {

    //timely::execute(timely::Configuration::Process(6),move |worker|{
    timely::execute_from_args(std::env::args(), move |worker| {

        let index = worker.index();
        let peers = worker.peers();

        //let (left_index, right_index) = get_worker_indices(data.read().unwrap().len(),index,peers);

        //let timer = Instant::now();
        for search_key in &keys {
            let mut input = InputHandle::new();

            worker.dataflow::<usize, _, _>(|scope| {
                let (handle, cycle) = scope.feedback(1);

                scope.input_from(&mut input)
                    .concat(&cycle)
                    .filter(move |x: &(usize, usize, bool, bool)| {
                        if (x.1 <= hop) { true } else { false }
                    })
                    .unary_frontier(
                        Pipeline,
                        "partitioned graph",
                        |cap, info|
                            {
                                move |input, output| {
                                    while let Some((a, b)) = input.next() {
                                        let mut session = output.session(&a);
                                        if index < (peers / 3) {
                                            //These are main threads. They have the name string.
                                            for temp in b.iter() {
                                                //println!("touple:{:?}",temp);
                                                let ind: usize = temp.deref().0;
                                                for i in ad_list.list[ind].iter() {
                                                    //println!("Nodes connected with: {} -> {}",ind,*i);
                                                    //session.
                                                    session.give((*i, temp.1 + 1, false, false));
                                                }
                                            }
                                        } else if (index - (peers / 3)) < (peers / 3) {
                                            for temp in b.iter() {
                                                //println!("In Th: {}, node: {:?}, touple:{:?}",index,node[temp.0],temp)
                                                let ind: usize = temp.deref().0;
                                                if nodes.records[ind].size > 10000 {
                                                    session.give((temp.0, temp.1, true, false));
                                                } else { session.give((temp.0, temp.1, false, true)); }
                                            }
                                        } else {
                                            for temp in b.iter() {
                                                //println!("node: {:?}, touple:{:?}",temp.0,temp.1);
                                                let ind: usize = temp.deref().0;
                                                if nodes.records[ind].friendliness > 5 {
                                                    session.give((temp.0, temp.1, true, true));
                                                } else { session.give((temp.0, temp.1, false, true)); }
                                            }
                                        }
                                    }
                                }
                            })
                    .inspect(move |x: &(usize, usize, bool, bool)| {
                        if (x.2 == true) && (x.3 == true) {
                            //Got the expected result
                            //                      println!("{:?}",nodes.records[x.0]);
                        }
                    })
                    .exchange(move |x: &(usize, usize, bool, bool)| {
                        let mut peer_in_group = 1;
                        if peers / 3 > 0 { peer_in_group = peers / 3; }

                        let step = 281903 / (peer_in_group);
                        let key = x.0;
                        let mut temp = key / step;
                        if temp == (peer_in_group) { temp -= 1; }

                        if (x.2 == false) && (x.3 == false) {
                            temp += (peers / 3);
                        } else if (x.2 == true) && (x.3 == false) {
                            temp += 2 * (peers / 3);
                        }
                        temp as u64
                    })
                    //.delay(|_,_| 100)
                    .connect_loop(handle);
                //.probe_with(&mut probe);
            });

            let mut peer_in_group = 1;
            if peers / 3 > 0 { peer_in_group = peers / 3; }
            let step = 281903 / (peer_in_group);
            let mut temp = search_key / step;
            if temp == (peer_in_group) { temp -= 1; }
            if (index == temp) {
                input.send((*search_key, 0 as usize, false, false));
            }
        }

    }).unwrap();
}

fn main() {
    let (ad_list,data,hop,keys) = get_data();

    let mut rng = rand::thread_rng();

    //let all_keys = (0..keys).map(|key| rng.gen_range(1u32, 281903u32) as usize).collect();
    let all_keys = get_keys(keys);

    let timer = Instant::now();
    search_partitioned(AdjListPtr::new(&ad_list), VerticesPtr::new(&data), all_keys, hop);
    println!("part_row: For searching: {} nodes with {} hop, it took {:?}",keys.len(),hop,timer.elapsed());
}