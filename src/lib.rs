extern crate timely;
extern crate csv;
#[macro_use]
extern crate serde_derive;

use abomonation_derive::Abomonation;
use std::sync::{RwLock, Arc};
use rand::Rng;
use timely::dataflow::operators::{Filter, Input, Map, Inspect, Probe, Operator};
use timely::dataflow::{InputHandle, ProbeHandle};
use std::ops::Deref;
use timely::dataflow::channels::pact::Exchange;
use std::collections::hash_map::DefaultHasher;
use std::hash::{Hash, Hasher};
use std::collections::HashMap;

#[macro_export]
macro_rules! create_pointer {
    ($pointer_name:ident,$pointertype:ident) => {
        #[derive(Debug, Clone, Copy, abomonation_derive::Abomonation)]
        pub struct $pointer_name(usize);

        impl $pointer_name {
            #[allow(clippy::ptr_arg)]
            pub fn new(cube: &$pointertype) -> Self {
                Self(cube as *const $pointertype as usize)
            }
        }

        impl std::ops::Deref for $pointer_name {
            type Target = $pointertype;

            fn deref(&self) -> &Self::Target {
                let p = self.0 as *const $pointertype;
                unsafe { &*p }
            }
        }
    };
}


#[derive(Abomonation, Debug, Clone, Deserialize, Eq, Ord, PartialEq, PartialOrd)]
pub struct Record{
    pub id: usize,
    pub size: usize,
    pub friendliness: usize,
}

#[derive(Debug, Clone, Deserialize)]
pub struct Link{
    pub first_node: usize,
    pub second_node: usize,
}

#[derive(Debug, Clone, Deserialize)]
pub struct search_node{
    pub search_nodes: usize,
}

pub struct AdjList {
    pub list: Vec<Vec<usize>>
}
create_pointer!(AdjListPtr,AdjList);

pub struct Vertices {
    pub records: Vec<Record>
}
create_pointer!(VerticesPtr,Vertices);

pub fn get_worker_indices(
    total_len: usize,
    worker_index: usize,
    worker_count: usize,
) -> (usize, usize) {
    let data_per_worker = total_len / worker_count;
    let left_index = data_per_worker * worker_index;
    let right_index = if (worker_index + 1) == worker_count {
        // Handle extra elements.
        total_len
    } else {
        data_per_worker * (worker_index + 1)
    };
    (left_index, right_index)
}

fn get_hashcode(key: String) -> u64 {
    let mut hasher = DefaultHasher::new();
    key.hash(&mut hasher);
    hasher.finish()
}


pub fn get_data() -> (AdjList,Vertices,usize,usize) {
    //command-line arguments. No of hops to search
    let hop: usize = std::env::args().nth(1).unwrap().parse().unwrap();
    let keys: usize = std::env::args().nth(2).unwrap().parse().unwrap();

    //Take edges from the file. Creates node if necessary. Then creates node attributes.
    let mut rdr = csv::Reader::from_path("web-Stanford.csv").unwrap();
    let mut rdr2 = csv::Reader::from_path("web-Stanford_node.csv").unwrap();

    let mut data= Vertices {records:Vec::new()};

    //let mut contain = HashMap::new();
    //let mut rng = rand::thread_rng();
    //let mut list = vec![0 as usize;data.len() as usize];
    //let mut ad_list= vec![[0usize; 1]; data.len()];
    let mut ad_list = AdjList { list: vec![Vec::new();282000 as usize]};
    let r1 = Record {id: 0, size: 0, friendliness: 0};
    //contain.insert(0,true);
    data.records.push(r1);

    for result in rdr2.deserialize() {
        let v: Record = result.unwrap();
        data.records.push(v);
    }

    for result in rdr.deserialize() {
        let link: Link = result.unwrap();

        //data.push(record);
//        if !contain.contains_key(&link.first_node) {
//            let size1 = rng.gen_range(500u32, 500000u32) as usize;
//            let fr1 = (rng.gen_range(0u32, 10u32) as usize);
//            let r1 = Record {id: link.first_node, size: size1, friendliness: fr1};
//            contain.insert(link.first_node,true);
//            data.records.push(r1);
//        }
//        if !contain.contains_key(&link.second_node) {
//            let size2 = rng.gen_range(500u32, 500000u32) as usize;
//            let fr2 = (rng.gen_range(0u32, 10u32) as usize);
//            let r2 = Record {id: link.second_node, size: size2, friendliness: fr2};
//            contain.insert(link.second_node,true);
//            data.records.push(r2);
//        }
        //data.push(r2);
        ad_list.list[link.first_node].push(link.second_node);
    }
    data.records.sort_by(|a, b| a.id.partial_cmp(&b.id).unwrap());
    (ad_list,data,hop,keys)
}

pub fn get_keys(keys:usize) -> Vec<usize> {
    let mut rdr;
    if keys == 500 {
        rdr = csv::Reader::from_path("key500.csv").unwrap();
    }
    else if keys == 1000 {
        rdr = csv::Reader::from_path("key1000.csv").unwrap();
    }
    else if keys == 2000 {
        rdr = csv::Reader::from_path("key2000.csv").unwrap();
    }
    else if keys == 5000 {
        rdr = csv::Reader::from_path("key5000.csv").unwrap();
    }
    else if keys == 10000 {
        rdr = csv::Reader::from_path("key10000.csv").unwrap();
    }
    else if keys == 15000 {
        rdr = csv::Reader::from_path("key15000.csv").unwrap();
    }
    else if keys == 20000 {
        rdr = csv::Reader::from_path("key20000.csv").unwrap();
    }
    else { rdr = csv::Reader::from_path("key5000.csv").unwrap(); } //Default value for # of keys

    let mut nodes:Vec<usize> = Vec::new();

    for result in rdr.deserialize() {
        let v: search_node = result.unwrap();
        nodes.push(v.search_nodes);
    }
    nodes
}